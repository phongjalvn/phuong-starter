const fs = require('fs')

var components = {}
var files = fs.readdirSync('./src/_includes/components').filter(function (x) {
  return x.substr(-3) == '.js'
})
for (var i = 0; i != files.length; ++i) {
  components[files[i]] = require(`./src/_includes/components/${files[i]}`)
}

module.exports = function (config) {
  config.setLiquidOptions({
    dynamicPartials: true,
  })

  // Static assets to pass through
  config.addPassthroughCopy('./src/public/fonts')
  config.addPassthroughCopy('./src/public/images')
  config.addPassthroughCopy('./src/favicon.ico')
  config.addPassthroughCopy('./src/manifest.json')
  config.addPassthroughCopy('./src/robots.txt')

  // 404
  config.setBrowserSyncConfig({
    callbacks: {
      ready: function (err, browserSync) {
        const content_404 = fs.readFileSync('dist/404.html')

        browserSync.addMiddleware('*', (req, res) => {
          // Provides the 404 content without redirect.
          res.write(content_404)
          res.end()
        })
      },
    },
  })

  // Components
  for (const comp in components) {
    const func = components[comp];
    func(config)
  }

  return {
    dir: {
      input: 'src',
      output: 'src/_tmp',
    },
    passthroughFileCopy: true,
    templateFormats: ['html', 'md', 'liquid', 'pug'],
    htmlTemplateEngine: 'liquid',
    dataTemplateEngine: 'liquid',
    markdownTemplateEngine: 'liquid',
  }
}
