module.exports = function (eleventyConfig) {
  // Universal Shortcodes (Adds to Liquid, Nunjucks, Handlebars)
  eleventyConfig.addShortcode(
    'abutton',
    function (text, link = '#', className = '') {
      return `<a class="btn-submit btn ${className}" href="${link}">
    ${text}
      <span>
      <svg class="w-6" fill="none" stroke="currentcolor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path d="m9 5l7 7-7 7" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
      </svg>
    </span>
  </a>`
    }
  )

  eleventyConfig.addShortcode(
    'atarget',
    function (text, target = '', className = '') {
      return `<a pl-target="${target}" class="btn-submit btn ${className}">
    ${text}
      <span>
      <svg class="w-6" fill="none" stroke="currentcolor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path d="m9 5l7 7-7 7" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
      </svg>
    </span>
  </a>`
    }
  )

  eleventyConfig.addShortcode(
    'button',
    function (text, link = '#', className = '') {
      return `<button class="outline-none btn-submit btn ${className}" href="${link}">
    ${text}
      <span>
      <svg class="w-6" fill="none" stroke="currentcolor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path d="m9 5l7 7-7 7" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
      </svg>
    </span>
  </button>`
    }
  )
}
