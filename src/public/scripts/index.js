import $ from 'jquery'
// eslint-disable-next-line import/no-named-as-default
import Swiper from 'swiper/bundle'

const $topNav = $('#top-bar, body')

const $wakodoClub = $('#wakodo-club')

let wakodoClubTimer = null
$(window)
  .on('load', function () {
    init()
  })
  // sticky menu
  .on('scroll', function () {
    const scrollTop = $(window).scrollTop()
    // console.log(scrollTop)
    // Sticky menu
    if (scrollTop > 100) $topNav.addClass('sticky')
    else $topNav.removeClass('sticky')

    // Wakodo Club banner
    const topEdge = 100
    if (scrollTop > topEdge) {
      $wakodoClub.addClass('sticky')

      // Clear timer first
      if (wakodoClubTimer) clearTimeout(wakodoClubTimer)

      wakodoClubTimer = setTimeout(() => {
        if ($(window).scrollTop() > topEdge) $wakodoClub.addClass('anim')
      }, 500)
    } else {
      $wakodoClub.removeClass('sticky anim')
    }
  })

const init = function () {
  if (document.querySelectorAll('.news-swiper').length > 0) {
    const mySwiper = new Swiper('.news-swiper', {
      slidesPerView: 1,
      spaceBetween: 20,
      pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
      },
      breakpoints: {
        // when window width is >= 320px
        320: {
          spaceBetween: 30,
        },
        // when window width is >= 480px
        480: {
          spaceBetween: 40,
        },
        // when window width is >= 640px
        640: {
          spaceBetween: 50,
        },
        1024: {
          spaceBetween: 100,
        },
      },
    })
    mySwiper.init()
  }

  if (document.querySelectorAll('.events-swiper').length > 0) {
    const eventslider = new Swiper('.events-swiper', {
      slidesPerView: 1,
      spaceBetween: 20,
      breakpoints: {
        640: {
          slidesPerView: 2,
        },
        960: {
          slidesPerView: 3,
        },
      },
    })
    eventslider.init()
  }

  if (document.querySelectorAll('.kv-slider').length > 0) {
    const kvslider = new Swiper('.kv-slider', {
      slidesPerView: 1,
      spaceBetween: 0,
      breakpoints: {
        640: {
          slidesPerView: 1,
        },
        960: {
          slidesPerView: 1,
        },
      },
    })
    kvslider.init()
  }

  if (document.querySelectorAll('cn-slider').length > 0) {
    const cnslider = new Swiper('.cn-slider', {
      slidesPerView: 1,
      spaceBetween: 0,
      breakpoints: {
        640: {
          slidesPerView: 1,
        },
        960: {
          slidesPerView: 1,
        },
      },
    })
    cnslider.init()
  }
}

$('ul.tabs li').on('click', function () {
  var tab_id = $(this).attr('data-tab')

  $('ul.tabs li').removeClass('current')
  $('.tab-content').removeClass('current')

  $(this).addClass('current')
  $('#' + tab_id).addClass('current')
})
// Global toggle methods
const $toggleBtns = $('[pl-target]')

$toggleBtns.on('click', function (e) {
  e.preventDefault()
  const $this = $(this)
  const target = $this.attr('pl-target')
  const noToggle = $this.attr('pl-no-toggle')
  const addClass = $this.attr('pl-class') || 'active'
  const addSelfClass = $this.attr('pl-self-class') || 'active'
  const $target = $(`#${target}`)

  if (!$target) return

  $this.toggleClass(`active ${addSelfClass}`)

  if (noToggle === undefined) $target.toggle()
  // !Cheat to make sure animation happen
  setTimeout(() => {
    $target.toggleClass(`active ${addClass}`)
  }, 0)
})

// Additional function to autofocus on search
const $searchBtn = $('#search button')
const $searchInput = $('#search input')

$searchBtn.on('click', function () {
  $searchInput.trigger('focus')
})

// Auto collapse
// $searchInput.on('blur', function () {
//   $searchBtn.trigger('click')
// })
