module.exports = {
  name: "Phuong's Starter Package",
  env: process.env.NODE_ENV,
  url:
    process.env.NODE_ENV === 'development'
      ? 'http://localhost:8080/'
      : 'http://localhost:8080/dist/',
}
