module.exports = {
  mount: {
    'src/_tmp': { url: '/', static: true, resolve: true },
    'src/public/scripts': { url: '/public/scripts' },
    'src/public/styles': { url: '/public/styles' },
  },
  plugins: [
    ['@snowpack/plugin-sass', {}],
    '@snowpack/plugin-postcss',
    [
      '@snowpack/plugin-run-script',
      {
        cmd: 'eleventy',
        watch: '$1 --watch',
      },
    ],
  ],
  packageOptions: {
    NODE_ENV: true,
    source: 'remote',
  },
  buildOptions: {
    clean: true,
    out: 'dist',
  },
  devOptions: {
    open: 'none',
  },
  optimize: {
    bundle: true,
    minify: true,
    target: 'es2020',
  },
  exclude: ['**/node_modules/**/*', '**/_*.scss', '/src/_tmp/**/*'],
}
